﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Oracle.DataAccess.Client;

namespace stockGamesSystem
{
    public partial class frmEntradaProduto : Form
    {
        Conexao con = new Conexao();
        public frmEntradaProduto()
        {
            InitializeComponent();
        }

        private void cmbProduto_SelectedIndexChanged(object sender, EventArgs e)
        {
            string sqlConsulta,teste;

            sqlConsulta = "select idproduto from produto where nome='mario'";

            OracleCommand cmd = new OracleCommand(sqlConsulta, con.getConnection());

            teste =Convert.ToString(cmd.ExecuteNonQuery());

            MessageBox.Show(teste);
        }

        private void btnAdicionar_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(cmbProduto.Text))
            {
                MessageBox.Show("Consulte o cliente que deseja alterar clicando no Campo Produto", "ACR Rental Car", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            
            string sqlQuery;

            sqlQuery = "insert into produto (idproduto,produto,fornecedor,quantidade,valor_unitario) select nvl(max(idproduto),0)+1,:prod,:forne,:qtde,:v_unit from produto";
            
            OracleCommand cmd = new OracleCommand(sqlQuery, con.getConnection());

            cmd.Parameters.Add(new OracleParameter("=:prod", cmbProduto.Text));
            cmd.Parameters.Add(new OracleParameter("=:forne", cmbFornecedor.Text));
            cmd.Parameters.Add(new OracleParameter("=:qtde", txtQuantidade.Text));
            cmd.Parameters.Add(new OracleParameter("=:v_unit", txtValorUnit.Text));

            cmd.ExecuteNonQuery();
            MessageBox.Show("Cliente alterado com sucesso", "ACR Rental Car", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void frmEntradaProduto_Load(object sender, EventArgs e)
        {
            //Esse evento no Form pega os nomes da tabela produto e coloca no comboBox
            DataTable dtTabelas = new DataTable();

            OracleDataAdapter da = new OracleDataAdapter("select * from produto", con.getConnection());

            da.Fill(dtTabelas);

            cmbProduto.DataSource = dtTabelas;
            cmbProduto.DisplayMember = "nome";
            cmbProduto.ValueMember = "";

            cmbFornecedor.DataSource = dtTabelas;
            cmbFornecedor.DisplayMember = "fornecedor";
            cmbFornecedor.ValueMember = "";


            cmbProduto.SelectedIndex = -1;
            cmbFornecedor.SelectedIndex = -1;
        }
    }
}
