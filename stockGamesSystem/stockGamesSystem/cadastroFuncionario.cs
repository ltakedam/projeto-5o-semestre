﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Oracle.DataAccess.Client;



namespace stockGamesSystem
{
    public partial class cadastroFuncionario : Form
    {
        Conexao con = new Conexao();
        public cadastroFuncionario()
        {
            InitializeComponent();
        }

        // subrotinas para Habilitar controles
        private void Hablitar()
        {
            cmbNome.Enabled = true;
            txtSobrenome.Enabled = true; ;
            txtEndereço.Enabled = true; ;
            txtNumero.Enabled = true; ;
            mskDta.Enabled = true; ;
            mskDtd.Enabled = true; ;
            txtEmail.Enabled = true; ;
            txtTelefone.Enabled = true; ;
            txtTelefone2.Enabled = true; ;
            txtCelular.Enabled = true; ;
            txtSenha.Enabled = true; ;
            cmbCargo.Enabled = true; ;
            txtSalario.Enabled = true; ;
        }

        // subrotinas para Desabilitar controles
        private void Desabilitar()
        {
            cmbNome.Enabled = false;
            txtSobrenome.Enabled = false;
            txtEndereço.Enabled = false;
            txtNumero.Enabled = false;
            mskDta.Enabled = false;
            mskDtd.Enabled = false;
            txtEmail.Enabled = false;
            txtTelefone.Enabled = false;
            txtTelefone2.Enabled = false;
            txtCelular.Enabled = false;
            txtSenha.Enabled = false;
            cmbCargo.Enabled = false;
            txtSalario.Enabled = false;
        }

        // subrotinas para limpar controles do formulario de funcionarios
        private void Limparformularios()
        {
            cmbNome.Enabled = false;
            txtSobrenome.Clear();
            txtEndereço.Clear();
            mskDta.Clear();
            mskDtd.Clear();
            txtEmail.Clear();
            txtTelefone.Clear();
            txtTelefone2.Clear();
            txtCelular.Clear();
            txtSenha.Clear();
            cmbCargo.Enabled = false;
            txtSalario.Clear();
        }

        //Metodo que verifica se os campos principais forram preenchidos
        private bool Validadados()
        {
            //verificar se o campo nome foi preenchicom algum valor
            if (string.IsNullOrEmpty(cmbNome.Text))
            {
                // Verificar de o NOME
                MessageBox.Show("Preencimento do campo é obrigatório", "Campo Nome",MessageBoxButtons.OK, MessageBoxIcon.Information);
                // limpar o campo Nome
                cmbNome.SelectedIndex = -1;
                return false;
            }

            // Verificar se SOBRENOME foi preenchido
            if (string.IsNullOrEmpty(txtSobrenome.Text))
            {
                // mensagem ao usuario
                MessageBox.Show("Preencimento do campo é obrigatório", "Campo Nome", MessageBoxButtons.OK, MessageBoxIcon.Information);
                // limpar o campo Nome
                txtSobrenome.Clear();
                return false;
            } 
           
            // Verificar se o ENDEREÇO foi digitado
            if (string.IsNullOrEmpty(txtEndereço.Text))
            {
                // mensagem ao usuario
                MessageBox.Show("Preencimento do campo é obrigatório", "Campo Nome", MessageBoxButtons.OK, MessageBoxIcon.Information);
                // limpar o campo Nome
                txtEndereço.Clear();
                return false;
            }

            // Verificar se o CELULAR foi digitado
            if (string.IsNullOrEmpty(txtCelular.Text))
            {
                // mensagem ao usuario
                MessageBox.Show("Preencimento do campo é obrigatório", "Campo Nome", MessageBoxButtons.OK, MessageBoxIcon.Information);
                // limpar o campo Nome
                txtCelular.Clear();
                return false;
            }

            // Verificar se o SENHA foi digitad
            if (string.IsNullOrEmpty(txtSenha.Text))
            {
                // mensagem ao usuario
                MessageBox.Show("Preencimento do campo é obrigatório", "Campo Nome", MessageBoxButtons.OK, MessageBoxIcon.Information);
                // limpar o campo Nome
                txtSenha.Clear();
                return false;
            }

            // Verificar se o CARGO foi digitado
            if (string.IsNullOrEmpty(cmbCargo.Text))
            {
                // mensagem ao usuario
                MessageBox.Show("Preencimento do campo é obrigatório", "Campo Nome", MessageBoxButtons.OK, MessageBoxIcon.Information);
                // limpar o campo Nome
                cmbCargo.SelectedIndex = -1;
                return false;
            }

            // Verificar se o SALARIO foi digitado
            if (string.IsNullOrEmpty(txtSalario.Text))
            {
                // mensagem ao usuario
                MessageBox.Show("Preencimento do campo é obrigatório", "Campo Nome", MessageBoxButtons.OK, MessageBoxIcon.Information);
                // limpar o campo Nome
                txtSalario.Clear();
                return false;
            }
            //se tofods os campos forem preenchidos realizae cadastro
            return true;
        }
        
        private void btnSalvar_Click(object sender, EventArgs e)
        {
            if (Validadados() == false)
            {
                return; // intenrope sub-rotina
            }

            // delcaração da variavel de para qardar intruções em Oracle
            string OracleQuery="";

            // cria conexão chamndo metodo getConectionda clase conexao
            OracleCommand command = new OracleCommand(OracleQuery); 
        }
    }
}
